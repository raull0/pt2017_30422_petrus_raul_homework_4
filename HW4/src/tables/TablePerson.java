package tables;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Person;

public class TablePerson {
	protected int selectedRow;
	protected Vector<String> collumns;
	protected DefaultTableModel model;
	protected JTable tabel;

	public void buildTableModel(List<Person> t) {
		collumns = new Vector<String>();
		collumns.add("id");
		collumns.add("name");
		collumns.add("email");

		Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
		for (Person t1 : t) {
			Vector<Object> o = new Vector<>();
			o.add(t1.getId());
			o.add(t1.getName());
			o.add(t1.getEmail());
			rows.add(o);
		}
		model = new DefaultTableModel();
		model.setColumnIdentifiers(collumns);
		model.setDataVector(rows, collumns);
		tabel.setModel(model);

	}

	public int getSelectedRow1() {
		tabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				tabel = (JTable) e.getSource();
				if (e.getClickCount() == 1) {
					selectedRow = tabel.getSelectedRow();
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				//
			}

		});
		return (int) tabel.getValueAt(selectedRow, 0);

	}

	public void setTabel(JTable tabel2) {
		this.tabel = tabel2;

	}

}
