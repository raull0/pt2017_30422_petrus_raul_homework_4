package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

public class BankTest {

	int idP = 0;
	int idA = 0;
	Bank bank = new Bank(1);
	Person pe1 = new Person(idP++, "Raul Petrus", "petrusraul@gmail.com");
	Person pe2 = new Person(idP++, "Raul", "raul@gmail.com");
	Account a1 = new SavingAccount(idA++, "Raul Petrus", 100);
	Account a2 = new SpendingAccount(idA++,"Raul", 100);
	List<Account> pa1 = new ArrayList<>();
	List<Account> pa2 = new ArrayList<>();
	HashMap<Person, List<Account>> hm = new HashMap<>();

	@Test
	public void testAdd() {
		if (!bank.getAccounts().values().isEmpty())
			assertTrue(!bank.getAccounts().values().contains(pe1));
		pa1.add(a1); // expected List<Account>
		hm.put(pe1, pa1);
		bank.addPerson(pe1);
		bank.addAccount(a1, pe1);
		assertTrue(bank.getAccounts().equals(hm));
	}

	@Test
	public void testRemove() {
		bank.addPerson(pe1);
		bank.addAccount(a1, pe1);
		bank.addPerson(pe2);
		bank.addAccount(a2, pe2);
		pa1.add(a1);
		hm.put(pe1, pa1);
		bank.removeAccount(a2, pe2);
		bank.removePerson(pe2);
		// System.out.println(hm);
		// System.out.println(bank.getAccounts());
		assertTrue(bank.getAccounts().equals(hm));

	}

	@Test
	public void opAccounts() {
		a1.withdraw(50);
		int r = 100 + 100 * 2 / 100 - 50;
		assertTrue(a1.getBalance() == r);

		a1.withdraw(50);
		// 52 + 104 / 100 - 50 = 2 + 1
		int r1 = ((int) r + r * 2 / 100 - 50);
		assertTrue(a1.getBalance() == r1);
		// too many withdrawals
		assertTrue(!a1.withdraw(50));

		a2.deposit(100);
		int r2 = 100 + 100;
		assertTrue(a2.getBalance() == r2);

		a2.withdraw(150);
		r2 = r2 - 150 / 100;
		r2 = r2 - 150;
		assertTrue(a2.getBalance() == r2);

	}
	@Test
	public void editPerson(){
		bank.addPerson(pe2);
		Person p = new Person(idP-1, "R   ", "raul@gmail.com");
		bank.editPerson(p, idP-1);
		bank.getAccounts().containsKey(p);
	}

}
