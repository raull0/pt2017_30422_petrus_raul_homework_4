package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Person implements Serializable {

	public Person(int id, String name, String email) {
		this.name = name;
		this.id = id;
		this.email = email;
	}

	private int id;
	private String name;
	private String email;

	public Person() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return "Person " + id + " [name=" + name + ", email=" + email + "]";
	}

	public int hashCode() {
		return 1001 - id % 1001 + id ^ 11 + id * id * 113;
	}

	public boolean equals(Person other) {

		if (this.getId() != other.getId())
			return false;
		if (!this.getName().equals(other.getName()))
			return false;
		if (!this.getEmail().equals(other.getEmail()))
			return false;

		return true;
	}
}
