package model;

public class SavingAccount extends Account {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int NO_WITHDRAWALS = 2;
	private static final int INTEREST = 2;
	private int withdrawals;
	private boolean deposit;

	public SavingAccount(int iD, String holder, int balance) {
		super(iD, 0, holder, balance);
		this.withdrawals = 0;
		deposit=false;
	}

	@Override
	public boolean deposit(int amount) {
		if (!deposit) {
			this.balance =this.balance+ amount;
			deposit = true;
			return true;
		} else
			return false;

	}

	@Override
	public boolean withdraw(int amount) {
		if (withdrawals < NO_WITHDRAWALS && this.balance >= amount) {
			this.balance = this.balance + this.balance * INTEREST / 100;
			this.balance = this.balance - amount;
			withdrawals++;
			return true;
		} else

			System.out.println("Too many withdrawals");
		return false;

	}
	public boolean getDep(){
		return this .deposit;
	}
	public int getW(){
		return this.withdrawals;
	}
	
}
