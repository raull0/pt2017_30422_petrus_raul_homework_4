	package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;import java.util.Map;

import bankProc.BankProc;

public class Bank implements BankProc, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<Person, List<Account>> accounts;
	public static final int MAX_ACC = 101;

	public Bank() throws ClassNotFoundException, IOException {
		accounts = new HashMap<>();
	}
	public Bank(int i){
		accounts = new HashMap<>();
	}

	public HashMap<Person, List<Account>> getAccounts() {
		return accounts;
	}

	public void setAccounts(HashMap<Person, List<Account>> accounts) {
		this.accounts = accounts;
	}

	public void addPerson(Person person) {
		assert isWellFormed() : "Invalid hashing";
		int sizeBefore = accounts.keySet().size();
		assert person != null : "Person is not allowed to be null";
		assert accounts.containsKey(person) == false : "Person is not in the list";
		accounts.put(person, new ArrayList<Account>());
		assert accounts.keySet().size() == sizeBefore + 1 : "The person was not added";
		assert accounts.containsKey(person) == true : "Person was not added";
		assert isWellFormed() : "Invalid hashing";
		generateReport();

	}

	public boolean isWellFormed() {
		for (Person p : accounts.keySet()) {
			int hCode = 1001 - p.getId() % 1001 + p.getId() ^ 11 + p.getId() * p.getId() * 113;
			if (!(p.hashCode() == hCode))
				return false;
		}
		return true;
	}

	public void removePerson(Person person) {
		assert isWellFormed() : "Invalid hashing";
		int sizeBefore = accounts.keySet().size();
		assert person != null : "Person is not allowed to be null";
		assert accounts.containsKey(person) == true : "Person is not in the list";
		accounts.keySet().remove(person);
		assert accounts.keySet().size() == sizeBefore - 1 : "The person was not removed";
		assert accounts.containsKey(person) == false : "Person was not removed";
		assert isWellFormed() : "Invalid hashing";
		generateReport();
	}

	public void editPerson(Person person, int id) {
		assert isWellFormed() : "Invalid hashing";
		assert person != null : "Person is not allowed to be null";
		assert accounts.containsKey(this.readPerson(id)) == true : "Person is not in the list";
		Person p1 = new Person();
		for (Person p : accounts.keySet()) {
			if (p.getId() == id) {
				p1 = p;
				p1.setName(person.getName());
				p1.setEmail(person.getEmail());
			}
		}

		assert accounts.containsKey(p1) == true : "Person was removed";
		assert isWellFormed() : "Invalid hashing";
		generateReport();
	}

	public void addAccount(Account account, Person person) {

		assert isWellFormed();
		int sizeBefore = accounts.get(person).size();
		assert person != null : "Person is not allowed to be null";
		assert account != null : "ACcount is not allowed to be null";
		assert accounts.containsKey(person);
		assert !accounts.get(person).contains(account) : "It already contains the account";
		accounts.get(person).add(account);
		generateReport();
		assert accounts.get(person).contains(account);
		assert accounts.get(person).size() == sizeBefore + 1;
		assert isWellFormed();
	}

	public void removeAccount(Account account, Person person) {
		assert isWellFormed();
		int sizeBefore = accounts.get(person).size();
		assert person != null : "Person is not allowed to be null";
		assert account != null : "ACcount is not allowed to be null";
		assert accounts.containsKey(person);
		assert accounts.get(person).contains(account);
		accounts.get(person).remove(account);
		assert !accounts.get(person).contains(account);
		assert accounts.get(person).size() == sizeBefore - 1;
		assert isWellFormed();
	}

	public Account readAccount(int id) {
		assert id >= 0;
		assert isWellFormed();
		Account a2 = new Account();
		for (List<Account> a1 : accounts.values())
			for (Account a : a1)
				if (a.getID() == id) {
					a2 = a;
				}
		assert a2 != null;
		assert isWellFormed();
		return a2;

	}

	public Person readPerson(int id) {
		assert id >= 0;
		assert isWellFormed();
		Person p1 = new Person();
		for (Person p : accounts.keySet())
			if (p.getId() == id)
				p1 = p;
		assert p1 != null;
		assert isWellFormed();
		return p1;

	}

	public Person readPerson(String id) {
		Person p1 = new Person();
		assert isWellFormed();
		for (Person p : accounts.keySet())
			if (p.getName() == id)
				p1 = p;
		assert p1 != null;
		assert isWellFormed();
		return p1;

	}

	public void deposit(int id, int amount) {
		assert isWellFormed();
		assert id >= 0 && amount >= 0;
		int iV = readAccount(id).getBalance();
		readAccount(id).deposit(amount);
		if (readAccount(id).getType()==0)
		if(!((SavingAccount) readAccount(id)).getDep())
		assert readAccount(id).getBalance() == iV + amount;
		assert isWellFormed();
		generateReport();
	}

	public void withdraw(int id, int amount) {
		assert isWellFormed();
		assert id >= 0 && amount >= 0;
		int iV = readAccount(id).getBalance();
		if(readAccount(id).getType()==1){
		readAccount(id).withdraw(amount);
		if( iV - amount>0)
		assert readAccount(id).getBalance() == iV - amount-amount/100;
		}
		else {
			readAccount(id).withdraw(amount);
			iV = iV + 2*iV/100;
			int w=((SavingAccount) readAccount(id)).getW();
			if(w<2)
			assert readAccount(id).getBalance() == iV - amount;
		}
		
		assert isWellFormed();
		generateReport();
	}

	public  void generateReport() {
		try {
			FileOutputStream fileOut = new FileOutputStream("C:/Users/ASUS/Downloads/workspace/HW4/da.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	public static  Bank getReport() throws ClassNotFoundException, IOException {
		Bank b = new Bank();
		FileInputStream fileIn = new FileInputStream("C:/Users/ASUS/Downloads/workspace/HW4/da.ser");
		ObjectInputStream in = new ObjectInputStream(fileIn);
		b = (Bank) in.readObject();
		fileIn.close();
		return b;
	}

}
