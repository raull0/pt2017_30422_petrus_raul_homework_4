package model;

import java.io.Serializable;

public class Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Account(int iD, int type, String holder, int balance) {
		ID = iD;
		this.type = type;
		this.holder = holder;
		this.balance = balance;
	}

	public Account() {
		// TODO Auto-generated constructor stub
	}

	private int ID;
	private int type;
	private String holder;
	protected int balance;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getHolder() {
		return holder;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [ID=" + ID + ", type=" + type + ", holder=" + holder + ", balance=" + balance + "]";
	}

	public boolean withdraw(int amount) {
		return false;
	}

	public boolean deposit(int amount) {
		return false;
	}
}
