package model;

public class SpendingAccount extends Account {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int FEE = 1;
	public SpendingAccount(int iD, String holder, int balance) {
		super(iD, 1, holder, balance);
	}

	@Override
	public boolean deposit(int amount) {
		this.balance = this.balance + amount;
		return true;
	}

	@Override
	public boolean withdraw(int amount) {
		
		if (this.balance - amount >= 0) {
			this.balance = this.balance - amount* FEE / 100;
			this.balance = this.balance - amount;
			return true;
		} else
			return false;

	}

}
