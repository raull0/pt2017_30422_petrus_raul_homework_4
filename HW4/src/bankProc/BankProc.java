package bankProc;

import model.Account;
import model.Person;

public interface BankProc {

	/*
	 * Adds a person to the hashmap type : void
	 * 
	 * @param person, to be added
	 * 
	 * @pre person != null;
	 * 
	 * @pre accounts.containsKey(person) == false
	 * 
	 * @post accounts.size() = accounts.size()+1
	 * 
	 * @post accounts.containsKey(person) == true
	 */
	public void addPerson(Person person);
	/*
	 * Removes a person from the hashmap type: void
	 * 
	 * @param person to be removed
	 * 
	 * @pre person != null;
	 * 
	 * @pre accounts.containsKey(person) == true
	 * 
	 * @post accounts.size() = accounts.size()-1;
	 * 
	 * @post accounts.containsKey(person) == false
	 */

	public void removePerson(Person person);

	/*
	 * Edites a person in the hashmap type: void
	 * 
	 * @param id of person to be edited
	 * 
	 * @param new attributes of the person
	 * 
	 * @pre person!=null
	 * 
	 * @ore accounts.containsKey(person)
	 * 
	 * @post accounts.containsKey(person)
	 * 
	 */
	public void editPerson(Person person, int id);

	/*
	 * Adds an account to the hashSet
	 * 
	 * @param account to be added
	 * 
	 * @param person which creates the account
	 * 
	 * @pre person !=null
	 * 
	 * @pre account!=null
	 * 
	 * @pre accounts.containsKey(person)
	 * 
	 * @pre accounts.values().contains(account)==false
	 * 
	 * @post accounts.values().contains(account)==true
	 * 
	 * @post accounts.size()== accounts.size()+1
	 */
	public void addAccount(Account account, Person person);

	/*
	 * Removes an account from the hashset type: void
	 * 
	 * @param account to be removed
	 * 
	 * @pre person != null;
	 * 
	 * @pre account !=null;
	 * 
	 * @pre accounts.containsKey(person) == true
	 * 
	 * @post accounts.size() = accounts.size()-1;
	 * 
	 * 
	 * @post accounts.containsKey(person) == false
	 */
	public void removeAccount(Account account, Person person);

	/*
	 * @Invariant each Person should be put at its computed hashcode
	 */
	public boolean isWellFormed();

	/*
	 * Returns the account corresponding with id
	 * 
	 * @param id
	 * 
	 * @pre id >=0
	 * 
	 * @post account != null
	 */
	public Account readAccount(int id);

	/*
	 * Returns the Person corresponding with id
	 * 
	 * @param id
	 * 
	 * @pre id >=0
	 * 
	 * @post account != null
	 */
	public Person readPerson(int id);

	/*
	 * Returns the account corresponding with id
	 * 
	 * @param id
	 * 
	 * @pre !id.equals("");
	 * 
	 * @post account != null
	 */
	public Person readPerson(String id);

	/*
	 * Deposits an amount of money at the Account with id = id
	 * 
	 * @oaram id where to deposit
	 * 
	 * @param amount to be stored
	 * 
	 * @pre id >= 0 amount >=0
	 * 
	 * @post readAccount(id).balance = initialBalance + amount
	 * 
	 */
	public void deposit(int id, int amount);

	/*
	 * Withdraw an amount of money from the Account with id = id
	 * 
	 * @oaram id from where to withdraw
	 * 
	 * @param amount to be withdrawed
	 * 
	 * @pre id >= 0 amount >=0
	 * 
	 * @post readAccount(id).balance = initialBalance - amount
	 * 
	 */
	public void withdraw(int id, int amount);

}
