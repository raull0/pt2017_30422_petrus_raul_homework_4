package presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import tables.TableAccount;
import tables.TablePerson;

public class Controller {
	MainFrame mf;
	PersonFrame pf;
	AccountFrame af;
	Bank bank;
	TableAccount ta;
	TablePerson tp;
	int idP;
	int idA;

	public Controller() throws ClassNotFoundException, IOException {
		try {
			bank = Bank.getReport();
		} catch (Exception e) {
			bank = new Bank();
		}
		;
		mf = new MainFrame();
		ta = new TableAccount();
		tp = new TablePerson();
		for (Person p : bank.getAccounts().keySet())
			if (idP < p.getId())
				idP = p.getId();
		idP = idP + 1;
		for (List<Account> p1 : bank.getAccounts().values())
			for (Account p : p1)

				if (idA < p.getID())
					idA = p.getID();
		idA = idA + 1;
		mf.getUser().addActionListener(e -> user());
		mf.getAccount().addActionListener(e -> account());

	}

	public void user() {

		pf = new PersonFrame();
		pf.setID(idP);
		tp.setTabel(pf.getTable());
		upgradePe();
		tp.getSelectedRow1();
		pf.getAddPerson().addActionListener(e -> addPerson());
		pf.getRemovePerson().addActionListener(e -> delPerson());
		pf.getEdit().addActionListener(e -> editPerson());

	}

	public void account() {
		af = new AccountFrame();
		ta.setTabel(af.getTabel());
		upgradeAcc();
		ta.getSelectedRow1();
		af.getAddAccount().addActionListener(e -> addAccount());
		af.getDelAccount().addActionListener(e -> removeAccount());
		af.getDeposit().addActionListener(e -> depositMoney());
		af.getWithdraw().addActionListener(e -> withdrawMoney());

	}

	public void addPerson() {

		Person p = new Person(idP++, pf.getNamecText(), pf.getEmailText());
		bank.addPerson(p);
		pf.setID(idP);
		upgradePe();

	}

	public void delPerson() {
		int id = tp.getSelectedRow1();

		if (bank.getAccounts().keySet().size() > 1) {
			bank.removePerson(bank.readPerson(id));
			upgradePe();

		} else
			System.out.println("Cannot delete last person");

	}

	public void editPerson() {
		int id1 = tp.getSelectedRow1();
		Person p1 = new Person(0, pf.getNamecText(), pf.getEmailText());
		bank.editPerson(p1, id1);
		upgradePe();

	}

	public void addAccount() {
		try {
			Person p = bank.readPerson(Integer.parseInt(af.getIDText()));
			Account a;
			if (Integer.parseInt(af.getTypeText()) == 0)
				a = new SavingAccount(idA++, p.getName(), 0);
			else
				a = new SpendingAccount(idA++, p.getName(), 0);
			bank.addAccount(a, p);
			upgradeAcc();
		} catch (Exception e) {
		}
		;
	}

	public void removeAccount() {
		Account a = new Account(0, 0, "", 0);
		try {

			int id = ta.getSelectedRow1();
			a = bank.readAccount(id);
			bank.removeAccount(a, bank.readPerson(a.getHolder()));
			upgradeAcc();

		} catch (Exception e) {
		}
		;
	}

	public void depositMoney() {
		int id1 = ta.getSelectedRow1();
		try {
			bank.deposit(id1, af.getAmount());
		} catch (Exception e) {
			System.out.println("input amount");
		}
		upgradeAcc();

	}

	public void withdrawMoney() {
		int id1 = ta.getSelectedRow1();
		try {
			bank.withdraw(id1, af.getAmount());
		} catch (Exception e) {
			System.out.println("input amount");
		}
		upgradeAcc();

	}

	public void upgradeAcc() {
		ArrayList<Account> acc = new ArrayList<>();
		for (List<Account> a : bank.getAccounts().values()) {
			for (Account a1 : a)
				acc.add(a1);
		}

		ta.buildTableModel(acc);
	}

	public void upgradePe() {
		tp.buildTableModel(bank.getAccounts().entrySet().stream().map(e -> e.getKey())
				.collect(Collectors.toCollection(ArrayList::new)));
	}

}
