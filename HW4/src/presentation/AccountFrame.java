package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class AccountFrame {
	private JFrame frame;
	private JPanel panel1;
	private JLabel ID;
	private JLabel type;
	private JTextField IDText;
	private JTextField typeText;
	private JButton addAccount;
	private JButton delAccount;
	private JButton deposit;
	private JButton withdraw;
	private JTable tabel;
	private JScrollPane sp;
	private JTextField amount;
	private Dimension dim;
	private Font font1;
	private Font font2;
	private Color da;
	public AccountFrame() {
		new Font("Verdana", Font.BOLD, 20);
		font1 = new Font("Verdana", Font.BOLD, 16);
		font2 = new Font("Verdana", Font.BOLD, 12);
		new Color(60, 179, 113);
		da = new Color(152, 251, 152);
		new Color(102, 205, 170);
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame = new JFrame("ACCOUNTS");
		panel1 = new JPanel();
		ID = new JLabel("UID ", SwingConstants.CENTER);
		type = new JLabel("TYPE ", SwingConstants.CENTER);
		IDText = new JTextField();
		typeText = new JTextField();
		addAccount = new JButton("ADD");
		delAccount = new JButton("DEL");
		deposit = new JButton("DEPOSIT");
		withdraw = new JButton("WITHDRAW");
		tabel = new JTable();
		amount = new JTextField();
		generateView();
		
	}

	public void generateView() {
		frame.setBounds(0, 0, 490, 550);
		frame.setVisible(true);
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);

		panel1.setBounds(0, 0, 490, 550);
		panel1.setLayout(null);
		panel1.setBackground(Color.white);
		frame.add(panel1);

		sp = new JScrollPane(tabel);
		sp.setBounds(15, 70, 450, 300);
		panel1.add(sp);
		tabel.setEnabled(true);
		
		amount.setBounds(175,380,100,25);
		amount.setFont(font2);
		amount.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(amount);
		
		ID.setBounds(30, 420, 90, 29);
		ID.setBackground(da);
		ID.setOpaque(true);
		ID.setFont(font1);
		panel1.add(ID);

		IDText.setBounds(130, 420, 200, 30);
		IDText.setFont(font2);
		IDText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(IDText);

		type.setBounds(30, 460, 90, 29);
		type.setBackground(da);
		type.setOpaque(true);
		type.setFont(font1);
		panel1.add(type);

		typeText.setBounds(130, 460, 200, 30);
		typeText.setFont(font2);
		typeText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(typeText);

		addAccount.setBounds(350, 420, 100, 29);
		panel1.add(addAccount);

		delAccount.setBounds(350, 460, 100, 29);
		panel1.add(delAccount);

		deposit.setBounds(20, 20, 200, 29);
		panel1.add(deposit);

		withdraw.setBounds(255, 20, 200, 29);
		panel1.add(withdraw);

	}

	public JButton getAddAccount() {
		return addAccount;
	}

	public JButton getDelAccount() {
		return delAccount;
	}

	public JButton getDeposit() {
		return deposit;
	}

	public JButton getWithdraw() {
		return withdraw;
	}

	public JTable getTabel() {
		return tabel;
	}

	public String getIDText() {
		return IDText.getText();
	}

	public String getTypeText() {
		return typeText.getText();
	}
	public	int getAmount(){
		return Integer.parseInt(this.amount.getText());
	}

}
