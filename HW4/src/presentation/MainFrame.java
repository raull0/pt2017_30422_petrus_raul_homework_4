package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainFrame {
	private JFrame frame;
	private JPanel panel1;
	private Dimension dim;
	private JButton account;
	private Font font2;
	private Color butoane;
	private JButton user;

	public MainFrame() {
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		new Font("Verdana", Font.BOLD, 16);
		font2 = new Font("Verdana", Font.BOLD, 12);
		new Color(152, 251, 152);
		butoane = new Color(102, 205, 170);
		user = new JButton("USER");
		frame = new JFrame("Main Frame");
		panel1 = new JPanel();
		account = new JButton("ACCOUNT");

		da();

	}

	private void da() {
		frame.setBounds(0, 0, 280, 170);
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
		frame.setVisible(true);
		frame.getDefaultCloseOperation();

		panel1.setBounds(0, 0, 280, 170);
		panel1.setLayout(null);
		panel1.setBackground(Color.white);
		frame.add(panel1);

		user.setBounds(20, 50, 100, 40);
		user.setFont(font2);
		user.setBackground(butoane);
		panel1.add(user);

		account.setBounds(140, 50, 100, 40);
		account.setFont(font2);
		account.setBackground(butoane);
		panel1.add(account);
	}

	public JButton getAccount() {
		return account;
	}



	public JButton getUser() {
		return user;
	}


}
