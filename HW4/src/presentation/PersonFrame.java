package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class PersonFrame {
	private JFrame frame;
	private JPanel panel1;
	private JLabel UID;
	private JLabel namec;
	private JLabel email;
	private JTextField namecText;
	private JTextField UIDText;
	private JTextField emailText;
	private JTable tabel;
	private JScrollPane sp;
	private JButton addPerson;
	private JButton removePerson;
	private JButton edit;
	private Dimension dim;
	private Font font1;
	private Font font2;
	private Color da;
	public PersonFrame() {
		font1 = new Font("Verdana", Font.BOLD, 16);
		font2 = new Font("Verdana", Font.BOLD, 12);
		new Color(60, 179, 113);
		da = new Color(152, 251, 152);
		new Color(102, 205, 170);
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame = new JFrame("CLIENT");
		panel1 = new JPanel();
		email = new JLabel("EMAIL", SwingConstants.CENTER);
		new JLabel("ACC", SwingConstants.CENTER);
		namec = new JLabel("NAME", SwingConstants.CENTER);
		UID = new JLabel("UID ", SwingConstants.CENTER);
		emailText = new JTextField();
		namecText = new JTextField();
		UIDText = new JTextField();
		tabel = new JTable();
		addPerson = new JButton("ADD");
		removePerson = new JButton("DEL");
		edit = new JButton("EDIT");
		genareteView();
	}

	public void genareteView() {

		frame.setBounds(0, 0, 490, 550);
		frame.setVisible(true);
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);

		panel1.setBounds(0, 0, 490, 550);
		panel1.setLayout(null);
		panel1.setBackground(Color.white);
		frame.add(panel1);

		UID.setBounds(30, 380, 90, 29);
		UID.setBackground(da);
		UID.setOpaque(true);
		UID.setFont(font1);
		panel1.add(UID);

		UIDText.setBounds(130, 380, 200, 30);
		UIDText.setFont(font2);
		UIDText.setHorizontalAlignment(JTextField.CENTER);
		UIDText.setEditable(false);
		panel1.add(UIDText);

		email.setBounds(30, 460, 90, 29);
		email.setBackground(da);
		email.setOpaque(true);
		email.setFont(font1);
		panel1.add(email);

		emailText.setBounds(130, 460, 200, 30);
		emailText.setFont(font2);
		emailText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(emailText);

		namec.setBounds(30, 420, 90, 29);
		namec.setBackground(da);
		namec.setOpaque(true);
		namec.setFont(font1);
		panel1.add(namec);

		namecText.setBounds(130, 420, 200, 30);
		namecText.setFont(font2);
		namecText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(namecText);

		addPerson.setBounds(350, 380, 100, 29);
		panel1.add(addPerson);

		removePerson.setBounds(350, 420, 100, 29);
		panel1.add(removePerson);

		edit.setBounds(350, 460, 100, 29);
		panel1.add(edit);

		sp = new JScrollPane(tabel);
		sp.setBounds(15, 30, 450, 300);
		tabel.setEnabled(true);
		panel1.add(sp);
	}

	public String getNamecText() {
		return namecText.getText();
	}

	public String getUIDText() {
		return UIDText.getText();
	}
	public void setID(int id){
		this.UIDText.setText(String.valueOf(id));
	}

	public String getEmailText() {
		return emailText.getText();
	}

	public JButton getAddPerson() {
		return addPerson;
	}

	public JButton getRemovePerson() {
		return removePerson;
	}

	public JButton getEdit() {
		return edit;
	}



	public JTable getTable() {
		return this.tabel;
	}

}
